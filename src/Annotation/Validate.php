<?php

declare(strict_types=1);

namespace Phpben\Imi\Validate\Annotation;

use Imi\Bean\Annotation\Base;
use Imi\Bean\Annotation\Parser;

/**
 * Validate注解
 * @Annotation
 * @Target("METHOD")
 * @Parser("\Imi\Bean\Parser\NullParser")
 *
 * @property null|string $class 验证器类名
 * @property null|string $scene 验证场景
 * @property string $var 注入变量名称
 * @property bool $security 安全过滤
 * @property bool $fitler 过滤多余参数
 */
#[\Attribute(\Attribute::TARGET_METHOD)]
class Validate extends Base
{
    /**
     * 默认参数名
     * @var string|null
     */
    protected ?string $defaultFieldName = 'class';

    /**
     * @param null|string $class
     * @param null|string $scene
     * @param string $var
     * @param bool $security
     * @param bool $fitler
     */
    public function __construct(?array $__data = null, ?string $class = null, ?string $scene = null, string $var = 'data', bool $security = true, bool $fitler = true)
    {
        parent::__construct(...\func_get_args());
    }
}
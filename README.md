### Imi-Validate

安装：composer require phpben/imi-validate<br><br>
抽离Thinkphp6的validate验证器，可以在Imi上使用 手动验证（看tp6官方文档）及 注解验证<br>
使用方法与tp无异<br><br>

 **注解使用方式** 

注解：@Validate<br>
参数：<br>
    var=注入到方法中的变量名称<br>
    class=验证器类名，默认会自动取Controller同级目录下的Validate/xxxValidate.php<br>
    scene=场景名称，默认使用验证器内的场景<br>
    security=安全过滤所有参数，设置为false则为原参数内容<br>
    fitler=是否过滤多余参数，假如验证器规则有username，password，但是接口传入了username，password，xxx ，默认会把xxx过滤掉，设为false为不过滤<br><br>

控制器<br>
使用Validate注解，验证后的数据会自动注入到方法的data参数中，注入的变量为智能数组对象 支持对象与数组调用<br><br>

```
<?php

declare(strict_types=1);

namespace ImiApp\ApiServer\Backend\Controller;

use Imi\Server\Http\Controller\HttpController;
use Imi\Server\Http\Route\Annotation\Action;
use Imi\Server\Http\Route\Annotation\Controller;
use Imi\Server\Http\Route\Annotation\Route;


use Phpben\Imi\Validate\Annotation\Validate;

/**
 * @Controller("/test/")
 */
class TestController extends HttpController
{
    /**
     * @Action
     * @Route(url="login",method="POST")
     *
     *
     * @Validate
     */
    public function login($data)
    {
        return $data;
    }
    
}
// 更多使用方式
// @Validate(var="params",class="ImiApp\ApiServer\Validate",scene="test",security="false",fitler="false")


```
<br><br>
验证器

```
<?php

declare(strict_types=1);

namespace ImiApp\ApiServer\Backend\Validate;

use Phpben\Imi\Validate\Validate;

class AuthValidate extends Validate
{
    protected $rule =   [
        'password'  => 'require',
        'as'  => 'require',
    ];

    protected $message  =   [
        'password.require' => '名称必须',
    ];

    // 场景设置，不设置则使用所有规则验证，与thinkphp设置无异
    protected $scene = [
        'login'=>['password']
    ];
}

```
<br>
就这么简单啦，手动验证看thinkphp文档即可